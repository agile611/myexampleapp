package com.itnove.tests;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

/**
 * Created by guillemhs on 2015-11-29.
 */
public class IonicAppTest {
    RemoteWebDriver driver;
    DesiredCapabilities caps = new DesiredCapabilities();
    String device;
    int deviceNum;

    @Before
    public void setUp() throws MalformedURLException {
        // by default, start android app
        device = "ios";
        deviceNum = 0;
        // try to set according to user requirement
        if (System.getProperty("device") != null) {
            device = System.getProperty("device");
        } else {
            System.out.println("device not defined, will use default = Android");
        }
        deviceNum = device.equalsIgnoreCase("Android") ? 0 : 1;

        // switch between different browsers, e.g. iOS Safari or Android Chrome
        // let's use the os name to differentiate, because we only use default browser in that os
        if (device != null && device.equalsIgnoreCase("Android")) {
            File app = new File("/Users/guillemhernandezsola/.jenkins/workspace/ionic-complete-pipeline/platforms/android/build/outputs/apk/android-debug.apk");
            DesiredCapabilities caps = DesiredCapabilities.android();
            caps.setCapability("deviceName", "Android Emulator");
            caps.setCapability("app", app.getAbsolutePath());
            caps.setCapability("platformVersion", "5.1");
            driver = new AndroidDriver(new URL("http://0.0.0.0:4723/wd/hub"), caps);
        } else {
            File app = new File("/Users/guillemhernandezsola/.jenkins/workspace/ionic-complete-pipeline/platforms/ios/build/emulator/myExampleApp.app");
            caps = DesiredCapabilities.iphone();
            caps.setCapability(MobileCapabilityType.APPIUM_VERSION, "1.6.3");
            caps.setCapability(MobileCapabilityType.BROWSER_NAME, "");
            caps.setCapability(MobileCapabilityType.PLATFORM_NAME, "iOS");
            caps.setCapability(MobileCapabilityType.AUTOMATION_NAME, "XCUITest");
            caps.setCapability(MobileCapabilityType.DEVICE_NAME, "iPhone 7");
            caps.setCapability(MobileCapabilityType.PLATFORM_VERSION, "10.3");
            caps.setCapability(MobileCapabilityType.APP, app.getAbsolutePath());
            caps.setCapability("xcodeOrgId", "3RYGCUBU3M");
            caps.setCapability("xcodeSigningId", "iPhone Developer");
            driver = new IOSDriver(new URL("http://0.0.0.0:4723/wd/hub"), caps);
        }
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }

    @After
    public void tearDown() {
        if (driver != null) {
            driver.quit();
        }
    }
}
