# Overview

This is the repo to play and test Jenkins Declarative Pipelines. It is a IONIC app.

You need the following plugins on your Jenkins 2.7+ instance:

* Pipeline: Declarative Agent API
* Build Pipeline Plugin
* Pipeline Maven Integration Plugin
* Pipeline: Multibranch with defaults
* Delivery Pipeline Plugin
* Sauce OnDemand plugin
* HTML Publisher plugin
* xUnit plugin


## Run IONIC
You will need Apache Cordova, Android SDK and XCODE install in your system.

To build the web, you execute the following command:

`ionic build browser`

To build the Android app, you execute the following command:

`ionic build android`

To build the IOS App, you execute the follwing command:

`ionic build ios`

## Jenkins Declarative Pipelines

You can find the related pipeline [here](https://bitbucket.org/itnove/jenkinsdeclarativepipelines/src/408b5b33c41d9fa1db23ee66f9bb5de125d174dd/09-ionic-pipeline-complete.groovy?at=master&fileviewer=file-view-default)


## Support

This tutorial is released into the public domain by ITNove under WTFPL.

[![WTFPL](http://www.wtfpl.net/wp-content/uploads/2012/12/wtfpl-badge-1.png)](http://www.wtfpl.net/)

This README file was originally written by [Guillem Hernández Sola](https://www.linkedin.com/in/guillemhernandezsola/) and is likewise released into the public domain.

Please contact ITNove for further details.

* ITNOVE a Cynertia Consulting
* Passeig de Gràcia 110, 4rt 2a
* 08008 Barcelona
* T: 93 184 53 44
